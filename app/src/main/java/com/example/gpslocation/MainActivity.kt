package com.example.gpslocation

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.gpslocation.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(), LocationListener {
    val LOCATION_PERM_CODE = 2
    lateinit var adapter: ArrayAdapter<*>
    var locationPermission: Boolean = false
    lateinit var locationManager: LocationManager
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setContentView(binding.root)


        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        // запрашиваем разрешения на доступ к геопозиции
        if ((ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            // переход в запрос разрешений
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERM_CODE
            )
        } else {
            locationPermission = true
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)

        val prv = locationManager.getBestProvider(Criteria(), true)
        val providers = locationManager.allProviders
        Log.d("my", locationManager.allProviders.toString())
        if (prv != null) {
            val location = locationManager.getLastKnownLocation(prv)
            if (location != null)
                displayCoord(location.latitude, location.longitude)
            Log.d("mytag", "location set")
        }

        if (locationPermission) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)

            adapter = object : ArrayAdapter<Any?>(
                this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                providers as List<Any?>
            ) {
                override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                    val view = super.getView(position, convertView, parent)
                    val text1 = view.findViewById<View>(android.R.id.text1) as TextView
                    val text2 = view.findViewById<View>(android.R.id.text2) as TextView

                    val provider = providers[position]

                    val isEnable = locationManager.isProviderEnabled(provider)

                    text1.text = provider

                    if (isEnable) {
                        text2.text = "Enabled"
                        text2.setTextColor(Color.parseColor("#00FF00"));
                    } else {
                        text2.text = "Disabled"
                        text2.setTextColor(Color.parseColor("#FF0000"));
                    }

                    return view

                }
            }
        } else {
            val requestDenied = arrayOf("Access not granted")
            adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, requestDenied)
        }
        binding.LisView.adapter = adapter
    }
    override fun onLocationChanged(loc: Location) {
        val lat = loc.latitude
        val lng = loc.longitude
        displayCoord(lat, lng)
        Log.d("my", "lat $lat long $lng")
        if (locationPermission) {
//            Log.d("my", "cur providers: ${locationManager.provider}")
        }
    }

    fun displayCoord(latitude: Double, longtitude: Double) {
        findViewById<TextView>(R.id.lat).text = String.format("%.5f", latitude)
        findViewById<TextView>(R.id.lng).text = String.format("%.5f", longtitude)
    }

    override fun onProviderDisabled(provider: String) {
        adapter.notifyDataSetChanged();
    }


    // TODO: обработать случай отключения GPS (геолокации) пользователем
    // onProviderDisabled + onProviderEnabled

    // TODO: обработать возврат в активность onRequestPermissionsResult

    override fun onProviderEnabled(provider: String) {
        adapter.notifyDataSetChanged();
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERM_CODE -> {
                // If request is cancelled, the result arrays are empty.
                locationPermission = if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d("my","Permissions accessed")
                    true
                } else {
                    Log.d("my","Permissions denied")
                    false
                }
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
                return
            }
        }
    }
}